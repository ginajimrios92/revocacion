#
# Author: Gina Jiménez
# Maintainer(s): GJ, OE, MS, AL, AF
# License: (c) Data Cívica 2020, GPL v2 or newer
#
# -----------------------------------------------
# revocacion/descriptives/src/grafs.r

#### Paquetería y directorios ####
if(!require(pacman)) install.packages("pacman")
pacman::p_load(tidyverse, here, sf, readxl, add2ggplot)
options(scipen=999)

files <- list(data1 = here("import-clean/output/revocacion.rds"),
              data3 = here("import-clean/output/censo-seccion.rds"),
              data2 = here("import-clean/output/diputaciones-2021.rds"),
              names=here("import-clean/input/names.xlsx"),
              logo = here("descriptives/share/logo-dc.png"))

#### Tema y funciones ####
source(here("descriptives/src/theme.R"))

save <- function(name){
  ggsave(paste0(here("descriptives/output/"), name, ".jpg"), width = 12, height = 8)
  ggsave(paste0(here("descriptives/output/"), name, ".svg"), width = 12, height = 8)
}

limpiar_edos <- function(x){
  x = gsub("Coahuila de Zaragoza","Coahuila", x)
  x = gsub("Veracruz de Ignacio de la Llave","Veracruz", x)
  x = gsub("Michoacán de Ocampo","Michoacán", x)
}

#### Importar datos ####
data <- readRDS(files$data1)
tempo <- readRDS(files$data2)

data <- left_join(data, tempo)
tempo <- readRDS(files$data3)
data <- left_join(data, tempo)

tempo <- read_excel(files$names)%>%
         clean_names()%>%
         mutate(cve_ent=formatC(cve_ent, width =2,
                                flag = "0", format = "d"))

data <- left_join(data, tempo)

rm(tempo)

#### Gráficas ####
data%>%
mutate(morena_gob=factor(morena_gob, levels=c(0,1),
                         labels=c("Gobernado por la oposición",
                                  "Gobernado por Morena")))%>%
ggplot(aes(x=total_morena, y=que_se_quede))+
  geom_abline(intercept = 0, slope = 1, linetype="dashed")+
  geom_point(aes(color=morena_gob), size = 3, alpha=.7) +
  scale_color_manual(values=colores)+
  labs(x="Votos en la unidad territorial por Morena o su coalición es 2021",
       y="Votos en la unidad territorial para que AMLO se quede",
       title="Votos por Morena (o su coalición) en 2021 vs \n Votos para que AMLO se quede en la Presidencia en 2022",
       subtitle="Por unidad territorial", color="",
       caption=caption1)+
  tema
save("scatter-21vs22-morena")
add_dclogo(graf = here("descriptives/output/scatter-21vs22-morena.png"),
           escala = 7)

data%>%
mutate(nom_ent=limpiar_edos(nom_ent),
       names=case_when(nom_ent=="Ciudad de México" ~ 1,
                        nom_ent=="México" ~ 2,
                        nom_ent=="Veracruz" ~ 3,
                        nom_ent=="Chiapas" ~ 4,
                        T ~ 5),
       names=factor(names, levels=c(1:5), labels=c("Ciudad de México",
                                    "Estado de México",
                                    "Veracruz",
                                    "Chiapas",
                                    "Otros")))%>%
ggplot(aes(x=total_morena, y=que_se_quede))+
geom_abline(intercept = 0, slope = 1, linetype="dashed")+
geom_point(aes(color = names), size = 3, alpha=.7) +
labs(x="Votos en la unidad territorial por Morena o su coalición es 2021",
     y="Votos en la unidad territorial para que AMLO se quede",
     title="Votos por Morena (o su coalición) en 2021 vs \n Votos para que AMLO se quede en la Presidencia en 2022",
     subtitle="Por unidad territorial", color="",
     caption=caption1)+
  scale_color_manual(values=colores2)+
tema
save("scatter-21vs22-ent")
add_dclogo(graf = here("descriptives/output/scatter-21vs22-ent.png"),
           escala = 7)


#### Barras ####
data%>%
  mutate(y21vs22=ifelse(total_morena>que_se_quede, 0, 1),
         y21vs22=factor(y21vs22, levels=c(0,1), 
                        labels=c("Morena obtuvo menos votos que en 2021",
                                 "Morena obtuvo más votos que en 2021")))%>%
group_by(y21vs22)%>%
summarize(vph_autom=mean(vph_autom, na.rm=T),
          vph_inter=mean(vph_inter, na.rm=T),
          pder_ss=mean(pder_ss, na.rm=T))%>%
pivot_longer(2:4, names_to="vars", values_to="per")%>%
mutate(vars=gsub("vph_autom", "Viviendas con automóvil", vars),
       vars=gsub("vph_inter", "Viviendas con internet", vars),
       vars=gsub("pder_ss", "Personas afiliadas a servicios de salud", vars))%>%
ggplot(aes(x=vars, y=per, fill=y21vs22))+
geom_bar(stat="identity", position="dodge", width=.7)+
scale_fill_manual(values=colores)+
labs(x="",
       y="Porcentaje",
       title="Características de las unidades territoriales según su voto por Morena en 2021 y 2022",
       subtitle="Porcentaje promedio por unidad territorial",
     fill="Votos de 2021 vs 2022*", caption="Fuente: Resultados elecciones 2021, revocación de mandato 2022 y estadísticas censales a escalas geoelectorales \n *Comparamos los votos que obtuvo Morena o sus coaliciones en 2021 vs los votos en la revocación en favor de que AMLO permanezca en la Presidencia")+
tema+
coord_flip()
save("bars1")
add_dclogo(graf = here("descriptives/output/bars1.png"),
           escala = 7)


data%>%
mutate(y21vs22=ifelse(total_morena>que_se_quede, 0, 1),
       y21vs22=factor(y21vs22, levels=c(0,1), 
                       labels=c("Morena obtuvo menos votos que en 2021",
                               "Morena obtuvo más votos que en 2021")))%>%
group_by(y21vs22)%>%
summarize(graproes=mean(graproes, na.rm=T),
          vph_snbien=mean(vph_snbien, na.rm=T))%>%
  pivot_longer(2:3, names_to="vars", values_to="per")%>%
  mutate(vars=gsub("vph_snbien", "Porcentaje de viviendas sin bienes**", vars),
         vars=gsub("graproes", "Grado promedio de escolaridad", vars))%>%
  ggplot(aes(x=vars, y=per, fill=y21vs22))+
  geom_bar(stat="identity", position="dodge", width=.7)+
  scale_fill_manual(values=colores)+
  labs(x="",
       y="",
       title="Características de las unidades territoriales según su voto por Morena en 2021 y 2022",
       subtitle="Promedio por unidad territorial",
       fill="Votos de 2021 vs 2022*", caption="Fuente: Resultados elecciones 2021, revocación de mandato 2022 y estadísticas censales a escalas geoelectorales \n *Comparamos los votos que obtuvo Morena o sus coaliciones en 2021 vs los votos en la revocación en favor de que AMLO permanezca en la Presidencia \n **Viviendas que no cuentan con refrigerador, lavadora, horno de microondas, automóvil, motocicleta, bicicleta, radio, televisor, computadora, internet,\n teléfono, celular, televisión de paga, ni consola de videojuegos")+
  tema+
  coord_flip()
save("bars2")
add_dclogo(graf = here("descriptives/output/bars2.png"),
           escala = 7)



data%>%
group_by(nom_ent, morena_gob)%>%
summarize(morena=sum(total_morena, na.rm=T),
          que_se_quede=sum(que_se_quede, na.rm=T))%>%
ungroup()%>%
mutate(dif=((que_se_quede/morena)-1)*100,
       nom_ent=limpiar_edos(nom_ent),
       morena_gob=factor(morena_gob, levels=c(0,1),
                         labels=c("Gobernado por la oposición",
                                  "Gobernado por Morena")))%>%
ggplot(aes(x=reorder(nom_ent, dif), y=dif, fill=morena_gob))+
geom_bar(stat="identity", position="dodge")+
labs(x="",
       y="Cambio porcentual entre 2022 y 2021",
       title="Votos de Morena en 2021 vs votos para que AMLO se quede en 2022*",
       subtitle="Por estado y según si el estado está gobernado por Morena",
       fill="", caption=caption1)+
  tema+
  scale_fill_manual(values=colores)+
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
save("bars-estados")
add_dclogo(graf = here("descriptives/output/bars-estados.png"),
           escala = 7)
#done
